package com.archi.controller.dto.request;

import javax.validation.constraints.NotEmpty;

public record AnimalToCreateRequestDto(@NotEmpty String name) {
}
