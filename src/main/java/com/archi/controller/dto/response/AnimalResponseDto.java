package com.archi.controller.dto.response;

public record AnimalResponseDto(String id, String name) {
}
