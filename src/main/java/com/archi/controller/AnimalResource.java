package com.archi.controller;

import com.archi.controller.dto.request.AnimalToCreateRequestDto;
import com.archi.controller.dto.response.AnimalResponseDto;
import com.archi.domain.Animal;
import com.archi.exception.AnimalAlreadyExistException;
import com.archi.mapper.AnimalMapper;
import com.archi.service.AnimalService;
import com.mongodb.MongoException;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.*;

@Path("/animals")
@Tag(name = "Animal")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AnimalResource {

    @Inject
    AnimalMapper animalMapper;

    @Inject
    AnimalService animalService;

    @POST
    @APIResponses(value = {
            @APIResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = AnimalResponseDto.class))),
            @APIResponse(responseCode = "400"),
            @APIResponse(responseCode = "500")
    })
    public Response create(@Valid AnimalToCreateRequestDto requestDto) {
        Animal animal = animalMapper.animalToCreateRequestDtoToAnimal(requestDto);

        try {
            animal = animalService.create(animal);
            return Response
                    .status(CREATED.getStatusCode())
                    .entity(animalMapper.animalToAnimalResponseDto(animal))
                    .build();
        } catch (AnimalAlreadyExistException ex) {
            return Response.status(BAD_REQUEST.getStatusCode()).build();
        } catch (MongoException ex) {
            return Response.status(INTERNAL_SERVER_ERROR.getStatusCode()).build();
        }
    }
}
