package com.archi.domain;

public record Animal(String id, String name) {
}
