package com.archi.mapper;

import com.archi.controller.dto.request.AnimalToCreateRequestDto;
import com.archi.controller.dto.response.AnimalResponseDto;
import com.archi.domain.Animal;
import com.archi.repository.dao.AnimalDao;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "cdi")
public interface AnimalMapper {
    @Mapping(target = "id", ignore = true)
    Animal animalToCreateRequestDtoToAnimal(AnimalToCreateRequestDto animalToCreateRequestDto);

    AnimalResponseDto animalToAnimalResponseDto(Animal animal);

    @Mapping(target = "id", ignore = true)
    AnimalDao animalToAnimalDao(Animal animal);

    @Mapping(source = "id", target = "id", qualifiedByName = "mapObjectIdToString")
    Animal animalDaoToAnimal(AnimalDao animalDao);

    @Named("mapObjectIdToString")
    default String mapObjectIdToString(ObjectId id) {
        if (id == null) {
            return null;
        } else {
            return id.toString();
        }
    }
}
