package com.archi.repository;

import com.archi.repository.dao.AnimalDao;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AnimalRepository implements PanacheMongoRepository<AnimalDao> {

    public boolean existByName(String animalName) {
        var animals = find("name", animalName);
        return animals.count() > 0;
    }
}
