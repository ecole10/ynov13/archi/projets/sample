package com.archi.service;

import com.archi.domain.Animal;
import com.archi.exception.AnimalAlreadyExistException;
import com.archi.mapper.AnimalMapper;
import com.archi.repository.AnimalRepository;
import com.archi.repository.dao.AnimalDao;
import com.mongodb.MongoException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class AnimalService {
    @Inject
    AnimalMapper animalMapper;

    @Inject
    AnimalRepository animalRepository;

    public Animal create(Animal animal) {
        AnimalDao animalDao = animalMapper.animalToAnimalDao(animal);

        if (animalRepository.existByName(animalDao.getName())) {
            throw new AnimalAlreadyExistException();
        }

        animalRepository.persist(animalDao);

        if (animalDao.id == null) {
            throw new MongoException("Animal " + animal.name() + " could not be created");
        }

        return animalMapper.animalDaoToAnimal(animalDao);
    }
}
