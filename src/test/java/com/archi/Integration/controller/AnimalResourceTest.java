package com.archi.Integration.controller;

import com.archi.controller.dto.request.AnimalToCreateRequestDto;
import com.archi.repository.AnimalRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusTest
public class AnimalResourceTest {

    @Inject
    AnimalRepository animalRepository;

    @BeforeEach
    public void setup() {
        animalRepository.deleteAll();
    }

    @Test
    public void createAnimal_shouldReturnAnAnimal() {
        given().contentType("application/json")
                .body(new AnimalToCreateRequestDto("garfield"))
                .when()
                .post("/animals")
                .then()
                .statusCode(CREATED.getStatusCode())
                .body("id", is(notNullValue()),
                        "name", is("garfield"));
    }

    @Test
    public void createAnimal_withEmptyName_shouldReturnAnError400() {
        given().contentType("application/json")
                .body(new AnimalToCreateRequestDto(""))
                .when()
                .post("/animals")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createAnimal_withExistingName_shouldReturnAnError400() {
        given().contentType("application/json")
                .body(new AnimalToCreateRequestDto("garfield"))
                .when()
                .post("/animals");

        given().contentType("application/json")
                .body(new AnimalToCreateRequestDto("garfield"))
                .when()
                .post("/animals")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }
}
